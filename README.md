VcsExtract tool
===============

Simple console tool exporting file versions from a version control system (SVN and GIT).

Primarily used to track changes in svn-versioned documents during all versions of a given file.

Supported VCS
-------------

* GIT
* SVN

License
-------
Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).
