package cz.sysrq.vcsextract;

import cz.sysrq.vcsextract.data.FileRevisionMeta;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.io.SVNRepository;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * SVN extractor service unit tests.
 *
 * Created by vsvoboda on 10.1.2016.
 */
public class SvnExtractorTest {

    // public read only TortoiseSVN repo for test
    private static final String URL = "http://svn.code.sf.net/p/tortoisesvn/code/Readme.md";
    private static final String USR = "";
    private static final String PWD = "";

    private SvnExtractor extractor = null;

    @Before
    public void setUp() throws Exception {
        extractor = new SvnExtractor();
    }

    @Test
    public void testGetRevisionsOf() throws Exception {
        List<SVNLogEntry> commitList = extractor.getRevisionsOf(URL, USR, PWD.toCharArray());
        Assert.assertNotNull(commitList);
        Assert.assertEquals(1, commitList.size());

//        for (SVNLogEntry commit : commitList) {
//            System.out.println("commit: " + commit);
//        }
    }

    @Test
    public void testExportRevision() throws Exception {
        long testRev = 26429L; // this must be a real revision number
        String testAuthor = "authorTest";
        Date testDate = new Date();
        String testMsg = "messageTest";
        SVNLogEntry svnLogEntry = new SVNLogEntry(Collections.emptyMap(), testRev, testAuthor, testDate, testMsg);

        // prepare target directory
        Path targetPath = Paths.get("target", "test-output").toAbsolutePath();
        Files.createDirectory(targetPath);

        // act
        FileRevisionMeta fileRevisionMeta = extractor.exportRevision(URL, svnLogEntry, USR, PWD.toCharArray(), targetPath);

        // assert
        Assert.assertNotNull(fileRevisionMeta);
        Assert.assertTrue(Files.exists(fileRevisionMeta.getFile()));
        Assert.assertEquals(String.format("%07d", testRev), fileRevisionMeta.getRevId());
        Assert.assertEquals(testAuthor, fileRevisionMeta.getRevAuthor());
        Assert.assertEquals(testDate, fileRevisionMeta.getRevDate());
        Assert.assertEquals(testMsg, fileRevisionMeta.getRevMsg());

        // clean-up
        Files.delete(fileRevisionMeta.getFile());
        Assert.assertFalse(Files.exists(fileRevisionMeta.getFile()));
        Files.delete(targetPath);
        Assert.assertFalse(Files.exists(targetPath));
    }

    @Test
    public void testExportRevisions() throws Exception {
        List<SVNLogEntry> commitList = extractor.getRevisionsOf(URL, USR, PWD.toCharArray());

        // prepare target directory
        Path targetPath = Paths.get("target", "test-output").toAbsolutePath();
        Files.createDirectory(targetPath);

        // act
        List<FileRevisionMeta> fileRevisionMetaList = extractor.exportRevisions(URL, commitList, USR, PWD.toCharArray(), targetPath);

        // assert
        Assert.assertEquals(1, fileRevisionMetaList.size());
        for (FileRevisionMeta fileRevisionMeta : fileRevisionMetaList) {
            Assert.assertTrue(Files.exists(fileRevisionMeta.getFile()));
        }

        // clean-up
        for (FileRevisionMeta fileRevisionMeta : fileRevisionMetaList) {
            Files.delete(fileRevisionMeta.getFile());
            Assert.assertFalse(Files.exists(fileRevisionMeta.getFile()));
        }
        Files.delete(targetPath);
        Assert.assertFalse(Files.exists(targetPath));
    }

    @Test
    public void testGetRepo() throws Exception {
        SVNRepository repository = SvnExtractor.getRepo(URL, USR, PWD.toCharArray());
        Assert.assertNotNull(repository);
        SVNURL svnurl = repository.getLocation();
        Assert.assertEquals("/p/tortoisesvn/code/Readme.md", svnurl.getPath());
    }
}