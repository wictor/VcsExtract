package cz.sysrq.vcsextract;

import cz.sysrq.vcsextract.data.FileRevisionMeta;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Git extractor service unit tests.
 *
 * Created by vsvoboda on 8.1.2016.
 */
public class GitExtractorTest {

    private GitExtractor extractor = null;

    @Before
    public void setUp() throws Exception {
        extractor = new GitExtractor();
    }

    @Test
    public void testGetRevisionsOf() throws Exception {
        String fileStr = "C:\\Work\\git-test\\sandbox\\README.md";
        File file = new File(fileStr);

        List<RevCommit> commitList = extractor.getRevisionsOf(file);

        Assert.assertNotNull(commitList);
        Assert.assertEquals(3, commitList.size());
        // the first is the oldest revision
        Assert.assertEquals("65ac26a7fca735dd3e92aeed64ff9085844e4a89", commitList.get(2).getName());
        Assert.assertEquals("3502e4791c9e28f933c474095d0028d1397a5b34", commitList.get(1).getName());
        Assert.assertEquals("68dc5d1f0fea3fc84af1b730603d60ac59f05007", commitList.get(0).getName());
    }

    @Test
    public void testExportRevision() throws Exception {
        String fileStr = "C:\\Work\\git-test\\sandbox\\README.md";
        File file = new File(fileStr);
        List<RevCommit> commitList = extractor.getRevisionsOf(file);

        // prepare target directory
        Path targetPath = Paths.get("target", "test-output").toAbsolutePath();
        Files.createDirectory(targetPath);

        // act
        FileRevisionMeta fileRevisionMeta = extractor.exportRevision(file, commitList.get(2), targetPath);

        // assert
        Assert.assertNotNull(fileRevisionMeta);
        Assert.assertTrue(Files.exists(fileRevisionMeta.getFile()));

        // clean-up
        Files.delete(fileRevisionMeta.getFile());
        Assert.assertFalse(Files.exists(fileRevisionMeta.getFile()));
        Files.delete(targetPath);
        Assert.assertFalse(Files.exists(targetPath));
    }

    @Test
    public void testExportRevisions() throws Exception {
        String fileStr = "C:\\Work\\git-test\\sandbox\\README.md";
        File file = new File(fileStr);
        List<RevCommit> commitList = extractor.getRevisionsOf(file);

        // prepare target directory
        Path targetPath = Paths.get("target", "test-output").toAbsolutePath();
        Files.createDirectory(targetPath);

        // act
        List<FileRevisionMeta> fileRevisionMetaList = extractor.exportRevisions(file, commitList, targetPath);

        // assert
        Assert.assertEquals(3, fileRevisionMetaList.size());
        for (FileRevisionMeta fileRevisionMeta : fileRevisionMetaList) {
            Assert.assertTrue(Files.exists(fileRevisionMeta.getFile()));
        }

        // clean-up
        for (FileRevisionMeta fileRevisionMeta : fileRevisionMetaList) {
            Files.delete(fileRevisionMeta.getFile());
            Assert.assertFalse(Files.exists(fileRevisionMeta.getFile()));
        }
        Files.delete(targetPath);
        Assert.assertFalse(Files.exists(targetPath));
    }

    @Test
    public void testTargetFilePath() throws Exception {
        String fileStr = "C:\\Work\\git-test\\sandbox\\README.md";
        File file = new File(fileStr);
        Path target = Paths.get("C:\\Work\\output");

        Path targetFilePath = GitExtractor.targetFilePath(file, "revisionId", target);

        Assert.assertEquals("C:\\Work\\output\\README-revisionId.md", targetFilePath.toString());
    }

    @Test
    public void testRelativeToRepoFlat() throws Exception {
        String fileStr = "C:\\Work\\git-test\\sandbox\\README.md";
        File file = new File(fileStr);

        FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
        try (Repository repository = repositoryBuilder.findGitDir(file).build()) {
            String relFilePath = GitExtractor.relativeToRepo(file, repository);

            Assert.assertEquals("README.md", relFilePath);
        }
    }

    @Test
    public void testRelativeToRepoDeeper() throws Exception {
        String fileStr = "C:\\Work\\git-test\\sandbox\\src\\main\\java\\cz\\sysrq\\gitlabtest\\Main.java";
        File file = new File(fileStr);

        FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
        try (Repository repository = repositoryBuilder.findGitDir(file).build()) {
            String relFilePath = GitExtractor.relativeToRepo(file, repository);

            Assert.assertEquals("src/main/java/cz/sysrq/gitlabtest/Main.java", relFilePath);
        }
    }
}