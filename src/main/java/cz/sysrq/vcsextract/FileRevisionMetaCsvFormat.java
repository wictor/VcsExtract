package cz.sysrq.vcsextract;

import cz.sysrq.vcsextract.data.FileRevisionMeta;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * CSV formatter for {@link cz.sysrq.vcsextract.data.FileRevisionMeta}
 * TODO VSV: escaping quotes
 * <p/>
 * Created by vsvoboda on 10.1.2016.
 */
public class FileRevisionMetaCsvFormat {

    private static final String DELIM_COMMA = ",";
    private static final String EOL = System.lineSeparator();

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final String delim;

    public FileRevisionMetaCsvFormat(String delim) {
        this.delim = delim;
    }

    public FileRevisionMetaCsvFormat() {
        this(DELIM_COMMA);
    }

    public String format(FileRevisionMeta meta) {
        return meta.getFile().getFileName().toString() + delim
                + meta.getRevAuthor() + delim
                + dateFormat.format(meta.getRevDate()) + delim
                + meta.getRevId() + delim
                + meta.getRevMsg();
    }

    public String header() {
        return "File" + delim
                + "Author" + delim
                + "Date" + delim
                + "Revision" + delim
                + "Message";
    }

    public String format(List<FileRevisionMeta> list) {
        StringBuilder sb = new StringBuilder();
        sb.append(header()).append(EOL);
        for (FileRevisionMeta meta : list) {
            sb.append(format(meta)).append(EOL);
        }
        return sb.toString();
    }
}
