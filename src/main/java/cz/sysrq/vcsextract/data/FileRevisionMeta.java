package cz.sysrq.vcsextract.data;

import java.nio.file.Path;
import java.util.Date;

/**
 * Exported file revision metadata bean.
 *
 * Created by vsvoboda on 8.1.2016.
 */
public class FileRevisionMeta {

    private final Path file;
    private final String revId;
    private final Date revDate;
    private final String revAuthor;
    private final String revMsg;

    public FileRevisionMeta(Path file, String revId, Date revDate, String revAuthor, String revMsg) {
        this.file = file;
        this.revId = revId;
        this.revDate = revDate;
        this.revAuthor = revAuthor;
        this.revMsg = revMsg;
    }

    public Path getFile() {
        return file;
    }

    public String getRevId() {
        return revId;
    }

    public Date getRevDate() {
        return revDate;
    }

    public String getRevAuthor() {
        return revAuthor;
    }

    public String getRevMsg() {
        return revMsg;
    }
}
