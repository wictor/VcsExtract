package cz.sysrq.vcsextract;

import cz.sysrq.vcsextract.data.FileRevisionMeta;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * SVN revision extractor service.
 *
 * Created by vsvoboda on 9.1.2016.
 */
public class SvnExtractor {

    public List<SVNLogEntry> getRevisionsOf(String url, String user, char[] password) throws SVNException {
        long startRevision = 0;
        long endRevision = -1; //HEAD (the latest) revision

        SVNRepository repository = getRepo(url, user, password);
        Collection logEntries = repository.log(new String[]{""}, null, startRevision, endRevision, true, true);

        List<SVNLogEntry> list = new LinkedList<>();
        for (Object logObject : logEntries) {
            if (logObject instanceof SVNLogEntry) {
                SVNLogEntry entry = (SVNLogEntry) logObject;
                list.add(entry);
            }
        }

        return list;
    }

    public FileRevisionMeta exportRevision(String url, SVNLogEntry rev, String user, char[] password, Path exportPath) throws SVNException {
        SVNRepository repository = getRepo(url, user, password);
        SVNClientManager ourClientManager = SVNClientManager.newInstance(null,
                repository.getAuthenticationManager());
        SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
        SVNURL svnurl = repository.getLocation();

        return exportRev(updateClient, svnurl, rev, exportPath);
    }

    public List<FileRevisionMeta> exportRevisions(String url, Iterable<SVNLogEntry> revs, String user, char[] password, Path exportPath) throws SVNException {
        SVNRepository repository = getRepo(url, user, password);
        SVNClientManager ourClientManager = SVNClientManager.newInstance(null,
                repository.getAuthenticationManager());
        SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
        SVNURL svnurl = repository.getLocation();

        LinkedList<FileRevisionMeta> metadataList = new LinkedList<>();
        for (SVNLogEntry rev : revs) {
            FileRevisionMeta fileRevisionMeta = exportRev(updateClient, svnurl, rev, exportPath);
            metadataList.add(fileRevisionMeta);
        }
        return metadataList;
    }

    private FileRevisionMeta exportRev(SVNUpdateClient updateClient, SVNURL svnurl, SVNLogEntry rev, Path exportPath) throws SVNException {
        SVNRevision svnRevision = SVNRevision.create(rev.getRevision());
        updateClient.doExport(svnurl, exportPath.toFile(), SVNRevision.UNDEFINED, svnRevision, null, true, SVNDepth.EMPTY);

        Path fileName = Paths.get(svnurl.getPath()).getFileName();
        Path exportedFilePath = exportPath.resolve(fileName);
        File exportedFile = exportedFilePath.toFile();

        String revId = String.format("%07d", rev.getRevision());
        File targetFile = GitExtractor.targetFilePath(exportedFile, revId, exportPath).toFile();

        boolean success = exportedFile.renameTo(targetFile);
        if (!success) {
            throw new RuntimeException("Rename failed!");
        }

        return new FileRevisionMeta(targetFile.toPath(), revId, rev.getDate(), rev.getAuthor(), rev.getMessage());
    }

    static SVNRepository getRepo(String url, String user, char[] password) throws SVNException {
        SVNRepository repository = SVNRepositoryFactory.create(SVNURL.parseURIEncoded(url));
        // TODO VSV: authManager can be a field
        ISVNAuthenticationManager authManager = SVNWCUtil.createDefaultAuthenticationManager(user, password);
        repository.setAuthenticationManager(authManager);
        return repository;
    }
}
