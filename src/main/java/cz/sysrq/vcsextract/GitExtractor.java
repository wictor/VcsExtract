package cz.sysrq.vcsextract;

import cz.sysrq.vcsextract.data.FileRevisionMeta;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Git revision extractor service.
 * <p/>
 * Created by vsvoboda on 8.1.2016.
 */
public class GitExtractor {

    // https://maximilian-boehm.com/hp2118/Use-Java-Library-JGit-to-Programmatically-Access-Your-Git-Respository.htm?ITServ=CY7c85c55eX152046422b7XY284c
    // -> https://github.com/maxboehm/EDASOMIND/tree/master/GitAccess

    // http://wiki.eclipse.org/JGit/User_Guide

    // http://www.codeaffine.com/2014/09/22/access-git-repository-with-jgit/

    /**
     * Retrieves a versioned-file revisions.
     *
     * @param file target versioned file
     * @return collection of file revisions
     * @throws IOException
     * @throws GitAPIException
     */
    public List<RevCommit> getRevisionsOf(File file) throws IOException, GitAPIException {
        FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
        try (Repository repository = repositoryBuilder.findGitDir(file).build()) {
            String fileRelativeToRepoEsc = relativeToRepo(file, repository);
            LogFollowCommand cmd = new LogFollowCommand(repository, fileRelativeToRepoEsc);
            List<RevCommit> log = cmd.call();
            // GIT log is ordered from the most recent entry, but we need to be consistent with ordering from the oldest
            Collections.reverse(log);
            return log;
        }
    }

    /**
     * Exports single file revision.
     *
     * @param file       target versioned file
     * @param rev        revision
     * @param exportPath export target directory
     * @return file revision metadata
     * @throws IOException
     */
    public FileRevisionMeta exportRevision(File file, RevCommit rev, Path exportPath) throws IOException {
        FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
        try (Repository repository = repositoryBuilder.findGitDir(file).build()) {
            String fileRelativeToRepoEsc = relativeToRepo(file, repository);

            Path targetFilePath = targetFilePath(file, rev.getName(), exportPath);
            return exportRev(targetFilePath, repository, fileRelativeToRepoEsc, rev);
        }
    }

    /**
     * Exports specified file revisions.
     *
     * @param file       target versioned file
     * @param revs       revisions
     * @param exportPath export target directory
     * @return file revisions metadata
     * @throws IOException
     */
    public List<FileRevisionMeta> exportRevisions(File file, Iterable<RevCommit> revs, Path exportPath) throws IOException {
        List<FileRevisionMeta> metadataList = new LinkedList<>();

        FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
        try (Repository repository = repositoryBuilder.findGitDir(file).build()) {
            String fileRelativeToRepoEsc = relativeToRepo(file, repository);

            for (RevCommit rev : revs) {
                Path targetFilePath = targetFilePath(file, rev.getName(), exportPath);
                FileRevisionMeta metadata = exportRev(targetFilePath, repository, fileRelativeToRepoEsc, rev);
                metadataList.add(metadata);
            }
        }
        return metadataList;
    }

    private FileRevisionMeta exportRev(Path targetFilePath, Repository repository, String fileRelativeToRepoEsc, RevCommit rev) throws IOException {
        Files.createFile(targetFilePath);

        try (FileOutputStream fop = new FileOutputStream(targetFilePath.toFile())) {
            ObjectReader reader = repository.newObjectReader();
            // determine treewalk by relative path
            TreeWalk treewalk = TreeWalk.forPath(reader, fileRelativeToRepoEsc, rev.getTree());
            // open object
            reader.open(treewalk.getObjectId(0)).copyTo(fop);
        }

        PersonIdent commitInf = rev.getCommitterIdent();
        return new FileRevisionMeta(targetFilePath, rev.getName(), commitInf.getWhen(), commitInf.getName(), rev.getShortMessage());
    }

    /**
     * Constructs single revision file export path using provided metadata.
     *
     * @param file       target versioned file
     * @param revId      file revision
     * @param exportPath export target directory
     * @return path constructed like {@code target/fileName-revId.fileExtension}
     */
    static Path targetFilePath(File file, String revId, Path exportPath) {
        String fileName = file.getName();
        String extension = "";
        int extensionPos = fileName.lastIndexOf('.');
        if (extensionPos > 1) {
            extension = fileName.substring(extensionPos + 1);
            fileName = fileName.substring(0, extensionPos);
        }

        return exportPath.resolve(String.format("%s-%s.%s", fileName, revId, extension));
    }

    /**
     * Computes relative file path to the repository base.
     * Backslashes are replaced by forward slashes (needed by jgit).
     *
     * @param file       target file path
     * @param repository Git repository
     * @return relative file path to the repository base with replaced backslashes
     */
    static String relativeToRepo(File file, Repository repository) {
        Path repoPath = Paths.get(repository.getDirectory().getAbsolutePath()).getParent();
        Path fileRelativeToRepo = repoPath.relativize(file.toPath());
        return fileRelativeToRepo.toString().replace("\\", "/");
    }
}
