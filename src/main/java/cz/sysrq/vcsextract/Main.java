package cz.sysrq.vcsextract;

import cz.sysrq.vcsextract.data.FileRevisionMeta;
import org.apache.commons.cli.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.revwalk.RevCommit;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * VCS Extract main entry point.
 *
 * Created by wictor on 2.1.2016.
 */
public class Main {

    public static void main(String[] args) {
        Options options = createApplicationOptions();
        CommandLine commandLine = null;
        CommandLineParser parser = new DefaultParser();
        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Argument error: " + e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(APP_NAME, USAGE_HDR, options, USAGE_FTR, true);
            System.exit(1);
        }

        if (commandLine != null) {
            File targetFile = null;
            if (commandLine.hasOption("f")) {
                targetFile = new File(commandLine.getOptionValue("f"));
            }
            String targetUrl = commandLine.getOptionValue("u");
            String user = commandLine.getOptionValue("user", "");
            String password = commandLine.getOptionValue("password", "");
            String historyRevCntStr = commandLine.getOptionValue("h", "0");
            String destDirStr = commandLine.getOptionValue("d");

            System.out.println("target file (GIT): " + targetFile);
            System.out.println("target URL (SVN): " + targetUrl);
            System.out.println("SVN user: " + user);
            System.out.println("SVN password: " + password);
            System.out.println("history revision count: " + historyRevCntStr);
            System.out.println("destination: " + destDirStr);

            boolean doExport = commandLine.hasOption("h") && commandLine.hasOption("d");
            int historyRevCnt = 0;
            if (doExport) {
                historyRevCnt = Integer.valueOf(historyRevCntStr);
            }

            FileRevisionMetaCsvFormat csvFormat = new FileRevisionMetaCsvFormat(";");
            if (targetFile != null) {
                GitExtractor extractor = new GitExtractor();
                try {
                    System.out.println("Retrieving log...");
                    List<RevCommit> revisions = extractor.getRevisionsOf(targetFile);

                    if (doExport) {
                        historyRevCnt = historyRevCnt > revisions.size() ? revisions.size() : historyRevCnt;
                        List<RevCommit> selectedRevisions = revisions.subList(revisions.size() - historyRevCnt, revisions.size());
                        System.out.println("Exporting...");
                        List<FileRevisionMeta> fileRevisionMetaList = extractor.exportRevisions(targetFile, selectedRevisions, Paths.get(destDirStr));
                        System.out.println(csvFormat.format(fileRevisionMetaList));
                    } else {
                        System.out.println("GIT log only:");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        for (RevCommit rev : revisions) {
                            PersonIdent commit = rev.getCommitterIdent();
                            System.out.println(String.format("%s : %s %s %s", rev.getName(), sdf.format(commit.getWhen()), commit.getName(), rev.getShortMessage()));
                        }
                    }

                } catch (IOException | GitAPIException e) {
                    e.printStackTrace();
                }
            } else if (targetUrl != null) {
                SvnExtractor extractor = new SvnExtractor();
                try {
                    System.out.println("Retrieving log...");
                    List<SVNLogEntry> revisions = extractor.getRevisionsOf(targetUrl, user, password.toCharArray());

                    if (doExport) {
                        historyRevCnt = historyRevCnt > revisions.size() ? revisions.size() : historyRevCnt;
                        List<SVNLogEntry> selectedRevisions = revisions.subList(revisions.size() - historyRevCnt, revisions.size());
                        System.out.println("Exporting...");
                        List<FileRevisionMeta> fileRevisionMetaList = extractor.exportRevisions(targetUrl, selectedRevisions, user, password.toCharArray(), Paths.get(destDirStr));
                        System.out.println(csvFormat.format(fileRevisionMetaList));
                    } else {
                        System.out.println("SVN log only:");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        for (SVNLogEntry rev : revisions) {
                            System.out.println(String.format("%d : %s %s %s", rev.getRevision(), sdf.format(rev.getDate()), rev.getAuthor(), rev.getMessage()));
                        }
                    }

                } catch (SVNException e) {
                    e.printStackTrace();
                }
            }

        } else {
            throw new IllegalStateException("Arguments object empty although no exception thrown");
        }
    }

    private static final String APP_NAME = "vcsextract";
    private static final String APP_JAR = APP_NAME + ".jar";
    private static final String APP_URL = "https://gitlab.com/wictor/VcsExtract";
    private static final String USAGE_HDR = "HEADER.";
    private static final String USAGE_FTR = "\nEXAMPLE:\n" +
            " 1. To do something:\n" +
            "   java -jar \"" + APP_JAR + "\" -f README.md\n\n" +
            APP_URL;

    static Options createApplicationOptions() {
        Options options = new Options();
        OptionGroup modeOptGrp = new OptionGroup();
        modeOptGrp.addOption(Option.builder("f").longOpt("file").hasArg().desc("target file in a git repository").build());
        modeOptGrp.addOption(Option.builder("u").longOpt("url").hasArg().desc("SVN url to the target file").build());
        modeOptGrp.setRequired(true);
        options.addOptionGroup(modeOptGrp);
        options.addOption(Option.builder().longOpt("user").hasArg().desc("SVN user name").build());
        options.addOption(Option.builder().longOpt("password").hasArg().desc("SVN password").build());
        options.addOption(Option.builder("h").longOpt("history").hasArg().desc("export # of last revisions").build());
        options.addOption(Option.builder("d").longOpt("dest").hasArg().desc("export destination path").build());
        return options;
    }

}
